import { Component, OnInit } from '@angular/core';
import { BarberDetai } from 'src/assets/json/barberdetails';
import{DetailsService} from '../details.service';
import { Router} from '@angular/router';
import {LocalStorageService} from '../local-storage.service';

@Component({
  selector: 'app-barber-details',
  templateUrl: './barber-details.component.html',
  styleUrls: ['./barber-details.component.css']
})
export class BarberDetailsComponent implements OnInit {

  constructor(private details:DetailsService,private router:Router,private localStorage:LocalStorageService){
  }

  obj:BarberDetai;
  
  ngOnInit() {
    this.details.getDetails().subscribe(response => {
      this.obj = response
        });
  }

  callScheduler(id){  
localStorage.setItem("barberid",id);
 this.router.navigate(['/scheduler'])
}


}
