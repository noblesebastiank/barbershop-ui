import { Injectable } from '@angular/core';

import{BookingData } from'../assets/json/BookingData';
import {Observable} from "rxjs";
import { of } from 'rxjs';
import{BarberDetai} from '../assets/json/barberdetails';
import{HttpClient}from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class DetailsService {

  constructor(private httpClient:HttpClient) { }

  barberid:any;

  getDetails(){
  return this.httpClient.get<BarberDetai>("http://localhost:3030/barber/details");
  } 
  getInfo(id){
    return this.httpClient.get<BookingData>("http://localhost:3030/booking/details/"+id);
  }

  send(Subject,startTime,endTime,name,phone,location,comments,barberId,barberName,IsBlock,bookingDate){
    return this.httpClient.post('http://localhost:3030/booking/create',{"bookingDate":bookingDate,"name":name,"phone":phone,"barberId":barberId,"barberName":barberName,"startTime":startTime,"endTime":endTime,"subject":Subject,"location":location,"comments":comments,"IsBlock":IsBlock}).subscribe((res:Response)=>{
      const variable=res;
   }

)
  }
}
