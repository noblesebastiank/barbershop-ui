import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerSchedulerComponent } from './customer-scheduler.component';

describe('CustomerSchedulerComponent', () => {
  let component: CustomerSchedulerComponent;
  let fixture: ComponentFixture<CustomerSchedulerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerSchedulerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerSchedulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
