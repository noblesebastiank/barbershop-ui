import { Component, OnInit, ViewChild} from '@angular/core';
import { L10n } from '@syncfusion/ej2-base';
import { View, EventSettingsModel, ScheduleComponent, PopupCloseEventArgs } from '@syncfusion/ej2-angular-schedule';
import { DataManager, ODataV4Adaptor, Query } from '@syncfusion/ej2-data';
import {LocalStorageService} from '../local-storage.service';
import{DetailsService} from '../details.service';
import{BookingData} from 'src/assets/json/BookingData';
L10n.load({
  'en-US': {
    'schedule': {
      'newEvent': 'Book an Appointment',
      'saveButton': 'Book Now'
    }
  }
});

@Component({
  selector: 'app-customer-scheduler',
  templateUrl: './customer-scheduler.component.html',
  styleUrls: ['./customer-scheduler.component.css']
})
export class CustomerSchedulerComponent implements OnInit {
  @ViewChild('scheduleObj')
  public scheduleObj: ScheduleComponent;
  
  constructor(private localStorage:LocalStorageService,private details:DetailsService) {
 
   }

  id :any;
  object:BookingData;
  ngOnInit(): void {
  this.id=localStorage.getItem("barberid");
    this.details.getInfo(this.id).subscribe(response => {
      this.object = response;
        this.localStorage.set("jsondata",this.object);
        });   
        
  }


  public setView: View = 'Week';
  public setViews: View[]=['Day', 'Week', 'Month'];
  public startHour: string = '09:00';
  public endHour: string = '19:00';
  

  public dateParser(data: string) {
    return new Date(data);
  }
//tempdata
 private dataQuery: Query = new Query().from("Events").addParams('Block', 'true');
  s:any=this.localStorage.get("jsondata");
 
    public eventSettings: EventSettingsModel = {
      dataSource: this.s,
      fields: {
        id: 'id',
        subject: { name: 'subject' },
        startTime: { name: 'startTime' },
        endTime: { name: 'endTime' }

      
    }
  };

  /*
    *dealing Fail events
  */
  onActionFailure(): void {
    let ele: HTMLElement = document.getElementById("warningSpan");
    if(ele == null){ 
      let span: HTMLElement = document.createElement('span');
      span.setAttribute("id","warningSpan");
      this.scheduleObj.element.parentNode.insertBefore(span, this.scheduleObj.element);
      span.style.color = '#FF0000'
      span.innerHTML = 'Server Exception Occured: 404 Not found';
    }
  }


  onCellClick(args) {
        args.cancel = true; // Prevents the display of quick window on clicking the cells.
  }

 public onPopupClose(args: PopupCloseEventArgs) {
  if(args.type === 'Editor') {
    if(["Add", "Save", "EditSeries", "EditOccurrence"].indexOf(this.scheduleObj.currentAction) > -1) {
      // Handle the code if "save" button is clicked. 
      let Subject = "Booked, Not Available";
      let startTime = (document.getElementById("startTime") as HTMLInputElement).value;
      let endTime = (document.getElementById("EndTime") as HTMLInputElement).value;
      let name = (document.getElementById("name") as HTMLInputElement).value;
      let phone = (document.getElementById("phone") as HTMLInputElement).value;
      let location = (document.getElementById("location") as HTMLInputElement).value;
      let comments = (document.getElementById("comments") as HTMLInputElement).value;
      let barberId = localStorage.getItem("barberid");
      let barberName = "";
      let IsBlock = true;
      let bookingDate = new Date();
      //call http and send the data
      
    this.details.send(Subject,startTime,endTime,name,phone,location,comments,barberId,barberName,IsBlock,bookingDate);
    } else if(this.scheduleObj.currentAction === null) {
      // Handle the code if "cancel" button is clicked.
    }
  }
}


}
