import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from'@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScheduleModule, RecurrenceEditorModule, WeekService, MonthService, DayService } from '@syncfusion/ej2-angular-schedule';
import { HomePageComponent } from './home-page/home-page.component';
import { BarberDetailsComponent } from './barber-details/barber-details.component';
import { CustomerSchedulerComponent } from './customer-scheduler/customer-scheduler.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    BarberDetailsComponent,
    CustomerSchedulerComponent,
    FooterComponent,
    HeaderComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ScheduleModule, RecurrenceEditorModule
  ],
  providers: [WeekService, MonthService, DayService,HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
