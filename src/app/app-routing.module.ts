import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{HomePageComponent} from './home-page/home-page.component';
import{BarberDetailsComponent} from './barber-details/barber-details.component';
import {CustomerSchedulerComponent} from './customer-scheduler/customer-scheduler.component'

const routes: Routes = [ 
  { path: '', redirectTo: '/home', pathMatch: 'full' },
{ path: 'barber', component: BarberDetailsComponent },
{ path: 'home', component: HomePageComponent},
{ path: 'scheduler', component: CustomerSchedulerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
