export class BarberDetai{
    id: number;
    name: string;
    address: string;
    phoneNumber: string;
    experience: string;
    rating: string;
    chargePerSession: string;
    barberImg:string;
}