export class BookingData{
    id: number;
    bookingDate: string;
    name: string;
    phone: string;
    barberId: number;
    barberName: string;
    startTime: string;
    endTime: string;
    subject: string;
    location: string;
    comments: string;
    IsBlock: boolean;
}