# SalonBookingApplication
A spring-boot application for booking a reservation in a Salon that checks the availability of barbers, and if they are available, a reservation can be done.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Application Architecture

![](images/Architecture_BarberShop.jpg)

## Application UI

### Home page

![](images/barbershop_home.png)

### Barber Details page

![](images/barbershop_members.png)

### Calendar page

![](images/barbershop_calendar.png)

## Requirements

* [Angular](https://angular.io)
* JAVASCRIPT
* CSS
* [Docker](https://www.docker.com/)


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


## Docker build
```bash
docker build --rm -f "Dockerfile" -t barbershop-ui:latest "."
```

### How the docker image is build
* Base image for the docker container is `nginx:alpine`

* Files under `root directory ` are copied to `/usr/share/nginx/html` in the image

### How to run docker container

```bash
docker container run  -p 80:80 -d --rm --name barbershop-ui  barbershop-ui:latest